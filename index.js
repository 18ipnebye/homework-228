
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    // Гетери
    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary * 3;
    }

    // Сеттери
    set name(newName) {
        this._name = newName;
    }

    set age(newAge) {
        this._age = newAge;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    // Гетер
    get lang() {
        return this._lang;
    }

    // Сеттер
    set lang(newLang) {
        this._lang = newLang;
    }
}

let programmer1 = new Programmer('John', 30, 50000, 'JavaScript');
let programmer2 = new Programmer('Jane', 35, 60000, 'C++');
let programmer3 = new Programmer('Bob', 40, 70000, 'PHP');

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
